# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
	soma_anteriores = 0 # a soma dos números anteriores à "n" mais um indica a posição do ímpar desejado.
        num_anterior = 1
        i = 1
        for i in 1 : (n - 1)
                soma_anteriores = soma_anteriores + num_anterior
                num_anterior = num_anterior + 1
        end
        return ((soma_anteriores + 1) * 2) - 1 # a posição do ímpar desejado vezes dois menos um é igual ao ímpar desejado.
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
	soma_anteriores = 0		# a soma dos números anteriores à "m" mais um indica a posição do ímpar desejado.
	num_anterior = 1
	i = 1
	for i in 1 : (m - 1)
		soma_anteriores = soma_anteriores + num_anterior
		num_anterior = num_anterior + 1
	end
	impar = ((soma_anteriores + 1) * 2) - 1		# a posição do ímpar desejado vezes dois menos um é igual ao ímpar desejado.
	lista_impares = string(impar)*" "
	j = 1
	for j in 1 : (m - 1)		# este laço concatena os ímpares na variável lista_impares
		impar = impar + 2
		lista_impares = lista_impares*string(impar)*" "
	end
	print(m, " ", m^3, " ", lista_impares)
	println()
end

function mostra_n(n)
	i = 1
	for i in 1 : (n)
		imprime_impares_consecutivos(i)
	end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        println("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        println("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        println("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        println("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        println("Sua função retornou o valor errado para n = 21")
    end
    println("Fim dos testes")
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
